$(document).ready(function(){

  // Initialize SVG For Everybody
  svg4everybody();

  // Initialize promo swiper
  var promoSwiper = new Swiper('.promo-slider', {
    speed: 1000,
    autoplay: {
      delay: 5000,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    }
  });

  // Initialize service swiper
  var serviceSwiper = new Swiper('#service-slider', {
    speed: 1000,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    }
  });

  // Initialize certificates swiper
  var certificatesSwiper = new Swiper('.certificates-slider', {
    speed: 1000,
    autoHeight: true,
    slidesPerView: 4,
    loop: true,
    centeredSlides: true,
    spaceBetween: 30,
    autoplay: {
      delay: 3000,
    },
    breakpoints: {
      413: {
        slidesPerView: 1,
        spaceBetween: 15
      },
      767: {
        slidesPerView: 2,
        spaceBetween: 30
      },
      1199: {
        slidesPerView: 3,
        spaceBetween: 30
      }
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      dynamicBullets: true,
    }
  });

  // Initialize partners swiper
  var partnersSwiper = new Swiper('.partners-slider', {
    speed: 1000,
    slidesPerView: 5,
    loop: true,
    spaceBetween: 30,
    autoplay: {
      delay: 5000,
    },
    breakpoints: {
      413: {
        slidesPerView: 2,
        spaceBetween: 15
      },
      767: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      1199: {
        slidesPerView: 4,
        spaceBetween: 30
      }
    },
  });

  // Initialize projects lightgallery
  $('.s-promo-projects__list').lightGallery({
    selector: '.s-project-card',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false,
    download: false,
    counter: false
  });

  // Initialize certificates lightgallery
  $('.certificates-slider').lightGallery({
    selector: '.swiper-slide',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false,
    download: false,
    counter: false
  });

  $('.s-portfolio__list').lightGallery({
    selector: '.s-project-card',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false,
    download: false,
    counter: false
  });

  var $grid = $('.s-portfolio__list').isotope({
    itemSelector: '.s-project-card',
    layoutMode: 'fitRows'
  });

  $('.s-portfolio__filter').on( 'click', 'button', function() {
    var filterValue = $(this).attr('data-filter');
    $grid.isotope({
      filter: filterValue
    });

    var $gallery = $('.s-portfolio__list');
    $gallery.data('lightGallery').destroy(true);
    $gallery.lightGallery({
        selector: filterValue.replace('*','')
    });
  });
});